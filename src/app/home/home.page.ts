import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public masks: Object;

  public phoneNumber: string = "";
  public cardNumber: string = "";
  public cardExpiry: string = "";
  public orderCode: string = "";

  constructor() {
 
    this.masks = {
        phoneNumber: ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
        cardNumber: [/\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
        cardExpiry: [/[0-1]/, /\d/, '/', /[1-2]/, /\d/],
        orderCode: [/[a-zA-z]/, ':', /\d/, /\d/, /\d/, /\d/]
    };

  }

  save(){

    let unmaskedData = {
        phoneNumber: this.phoneNumber.replace(/\D+/g, ''),
        cardNumber: this.cardNumber.replace(/\D+/g, ''),
        cardExpiry: this.cardExpiry.replace(/\D+/g, ''),
        orderCode: this.orderCode.replace(/[^a-zA-Z0-9 -]/g, '')
    };

    console.log(unmaskedData);

  }

}
